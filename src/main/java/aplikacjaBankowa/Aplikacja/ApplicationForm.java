package aplikacjaBankowa.Aplikacja;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.file.Paths;

public class ApplicationForm extends JFrame{
    private JPanel mainPanel;
    private JPanel panelBtnAdd;
    private JPanel panelBtnClear;
    private JButton btnAdd;
    private JButton btnClear;
    private JPanel panelEmpty;
    private JPanel panelApplication;
    private JPanel panelLabel;
    private JLabel labelApplication;
    private JPanel panelButtons;
    private JPanel labelApplicantData;
    private JPanel panelNameLabels;
    private JLabel labelName;
    private JLabel labelSurname;
    private JLabel labelPesel;
    private JLabel labelAccType;
    private JTextField textName;
    private JTextField textSurname;
    private JTextField textPesel;
    private JPanel panelText;
    private JPanel panelTxtName;
    private JPanel panelTextSurname;
    private JPanel panelTextPesel;
    private JPanel panelTextAccType;
    private JComboBox cmbAccType;
    private File file;


    public ApplicationForm() {
        setContentPane(mainPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Application");
        setPreferredSize(new Dimension(500,600));
        file =new File("test.txt");
        String[] options = new String[]{"",AccType.BUSINESS_ACCOUNT.getFieldName(),AccType.PERSONAL_ACCOUNT.getFieldName(),AccType.SAVING_ACCOUNT.getFieldName()};
        for (String option : options) {
            cmbAccType.addItem(option);
        }

        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!textName.getText().equals("") && !textSurname.getText().equals("")&& !textPesel.getText().equals("") && !cmbAccType.getSelectedItem().equals("")) {
                    Application application = new Application();
                    application.setName(textName.getText());
                    application.setSurname(textSurname.getText());
                    application.setPesel(textPesel.getText());
                    String selectedItem = cmbAccType.getSelectedItem().toString();
                    AccType[] accTypes = AccType.values();
                    for (AccType type:accTypes) {
                        if (type.getFieldName().equals(selectedItem)) {
                            application.setAccType(type);
                        }
                    }
                    try (PrintWriter writer = new PrintWriter(new FileWriter(file,true))){
                        writer.println(application.toString());
                        writer.println();
                        clearData();

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }else {
                    JOptionPane.showMessageDialog(null,"Please fill all the fields");
                }
            }
        });
        pack();
        btnClear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearData();
            }
        });
    }
    private void clearData(){
        textName.setText("");
        textSurname.setText("");
        textPesel.setText("");
        cmbAccType.setSelectedIndex(0);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ApplicationForm().setVisible(true);
            }
        });
    }
}
