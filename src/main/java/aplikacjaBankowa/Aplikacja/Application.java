package aplikacjaBankowa.Aplikacja;

public class Application {
    private String name;
    private String surname;
    private String pesel;
    private AccType accType;

    public Application() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public AccType getAccType() {
        return accType;
    }

    public void setAccType(AccType accType) {
        this.accType = accType;
    }

    @Override
    public String toString() {
        return  "name=" + name +
                ", surname=" + surname +
                ", pesel=" + pesel +
                ", accType=" + accType;
    }
}
