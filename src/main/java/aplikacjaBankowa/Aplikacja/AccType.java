package aplikacjaBankowa.Aplikacja;

public enum AccType {
    PERSONAL_ACCOUNT("Konto Osobiste"),BUSINESS_ACCOUNT("Konto biznesowe"),SAVING_ACCOUNT("Konto oszczędnościowe");

    private String fieldName;

    AccType(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }
}
