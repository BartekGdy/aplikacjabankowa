package aplikacjaBankowa.Jan;

import java.io.*;
import java.io.FileReader;
import java.util.HashMap;

/**
 * Created by jan_w on 20.09.2017.
 */
public class Client {

    private String name;
    private String surname;
    private String pesel;
    private String accountType;

    File file = new File("test.txt");

    public void readDataFromFile (File file) {
        try (BufferedReader reader = new BufferedReader(new FileReader(file))){
            String lineFromFile = "";
            HashMap<String, Client> mapa = new HashMap<>();
            Client client = new Client();

            do {
                lineFromFile = reader.readLine();

                if (lineFromFile == null){
                    break;
                }
                else if (lineFromFile.startsWith("name=")){
                    //String name = lineFromFile.replace("name=","");
                    client.setName(lineFromFile.replace("name=",""));
                }
                else if (lineFromFile.startsWith("surname=")){
                    //String surname = lineFromFile.replace("surname=", "");
                    client.setSurname(lineFromFile.replace("surname=", ""));
                }
                else  if (lineFromFile.startsWith("pesel=")){
                    //String pesel = lineFromFile.replace("pesel=", "");
                    client.setPesel(lineFromFile.replace("pesel=", ""));
                }
                else if (lineFromFile.startsWith("accountType=")){
                    //String accountType = lineFromFile.replace("accountType=", "");
                    client.setAccountType(lineFromFile.replace("accountType=", ""));
                }

                mapa.put(client.getPesel(), client);

            }while (lineFromFile != null);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Client() {
    }

    public Client(String name, String surname, String pesel, String accountType) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.accountType = accountType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
}
